﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SkinSetter : MonoBehaviour {

	[SerializeField]
	private Sprite[] m_Skins;

	void Start ()
	{
		int skinIndex = PlayerPrefs.GetInt("SkinID");
		GetComponent<SpriteRenderer>().sprite = m_Skins[skinIndex];
	}
	

}
