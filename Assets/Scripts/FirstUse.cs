﻿using UnityEngine;

[RequireComponent(typeof(PlayerSelect))]
public class FirstUse : MonoBehaviour
{
    private void Start()
    {
        if(!(PlayerPrefs.GetInt("FirstUse")==1))
        {
            GetComponent<PlayerSelect>().OnPLayerSelected();
        }

    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.R) && Application.isEditor)
        {

            PlayerPrefs.DeleteAll();
            GetComponent<PlayerSelect>().OnPLayerSelected();
        }

        if (Input.GetKey(KeyCode.C) && Application.isEditor)
        {

            PlayerPrefs.SetInt("currencyCoins", PlayerPrefs.GetInt("currencyCoins") + 1000000);
        }

    }


}
