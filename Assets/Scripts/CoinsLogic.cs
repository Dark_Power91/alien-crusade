﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsLogic : MonoBehaviour {

	#region Serialized fields
	[SerializeField]
	private Text coinsText;
	[SerializeField]
	private int CoinsPerKill; //TODO change to a multiplier.
	#endregion

	#region Private Fields
	private int coins;
	#endregion

	void Start ()
	{
		coins = PlayerPrefs.GetInt("currencyCoins");
		coinsText.text = "" + coins;

        CoinsPerKill = PlayerPrefs.GetInt("Coins");

        EnemyManager em = FindObjectOfType<EnemyManager>();
        if (em != null)
        {
            em.onEnemyDestroyed += AddMoney;
        }
	}
	
	private void AddMoney()
	{
		coins += CoinsPerKill;
		coins = Mathf.Clamp(coins, 0, 999999999);
		coinsText.text = "" + coins;
		PlayerPrefs.SetInt("currencyCoins", coins);
	}

    void OnDisable()
    {

    EnemyManager em = FindObjectOfType<EnemyManager>();
        if (em != null)
        {
            em.onEnemyDestroyed -= AddMoney;
        }

    }

}
