﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalOscillation : MonoBehaviour {

	#region Serialized fields
	[Header("Vertical Movement")]
    [SerializeField]
    private float m_Amplitude = 0.1f;
    [SerializeField]
    private float m_Frequency = 0.7f;
	#endregion

	#region Private fields
    private Vector3 m_center;
    private float m_accumulator = 0.0f;
    private Vector3 m_newPos;
	#endregion

	void Start () 
    {
        m_center = transform.position;
        m_newPos = transform.position;
	}
	
	void Update () 
    {
        m_accumulator += Time.unscaledDeltaTime;
        m_accumulator %= 360;
        m_newPos.x = transform.position.x;
        float sin = Mathf.Sin(2* Mathf.PI * m_Frequency * m_accumulator);
        m_newPos.y = m_center.y + (m_Amplitude * sin);
		m_newPos.z = transform.position.z;
        transform.position = m_newPos;
	}
}
