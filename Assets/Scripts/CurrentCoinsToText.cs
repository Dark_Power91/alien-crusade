﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Text))]
public class CurrentCoinsToText : MonoBehaviour {

    private Text m_CoinText;

    private void Start()
    {
        m_CoinText = GetComponent<Text>();
    }

    void Update () { //TODO Not so good
        m_CoinText.text = "" + PlayerPrefs.GetInt("currencyCoins");
	}
}
