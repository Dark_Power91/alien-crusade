﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerManager : EnemyManager
{
    #region Serialized fields
    [Header("Tower Specific Fields")]
    [SerializeField]
    private Canvas Healtbar;
    #endregion

    protected override IDamageable AddDamagable(GameObject i_enemy)
    {
        Tower tTower = i_enemy.AddComponent<Tower>();
        //Canvas c = Instantiate(Healtbar, Vector3.zero, Quaternion.identity);
        //c.transform.SetParent(tTower.transform,false);
        //Image i = c.GetComponentInChildren<Image>();
        //if(i!=null)
        //{
        //    tTower.Healthbar = i;
        //}
        //else
        //{
        //    Debug.LogError("Tower Canvas require an Image as Healtbar");
        //}
        return tTower;
    }

}
