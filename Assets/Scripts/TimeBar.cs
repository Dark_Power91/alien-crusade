﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//public delegate void OnLifeEnded(Tower i_Tower);
public delegate void OnTimeEnded();

[RequireComponent(typeof(Slider))]
public class TimeBar : MonoBehaviour {

	#region Serialized fields
	[SerializeField]
	[DisallowEditInPlayMode]
	private float m_startTimeAmount = 30;
	[SerializeField]
	private float m_timeToAdd = 1f;
	[SerializeField]
	private Text m_timeText;
	#endregion

	#region Private Fields
	private Slider timeBar;
	private float time;
	private bool called = false;
	#endregion

	#region Public Fields
	public event OnTimeEnded onTimeEnded = null;
	#endregion

	void Start () {
		timeBar = GetComponent<Slider>();
		time = m_startTimeAmount;
		m_timeToAdd = PlayerPrefs.GetFloat("Time");

		EnemyManager em = FindObjectOfType<EnemyManager>();
		if(em != null)
		{
			em.onEnemyDestroyed += addTime;
		}

	}
	
	void Update () {
		if (time > 0)
		{
			time -= Time.deltaTime;
			updateTimeBar();
		}
		else
		{
			TimeEndedEvent();
		}

	}

	private void addTime()
	{
		time += m_timeToAdd;
		updateTimeBar();
	}

	private void updateTimeBar()
	{
		time = Mathf.Clamp(time, 0, m_startTimeAmount);
		timeBar.normalizedValue = Mathf.Clamp01(time / m_startTimeAmount);
		m_timeText.text = "Time : " + time.ToString("F");
	}

	private void TimeEndedEvent()
	{
		if(onTimeEnded != null && !called)
		{
			called = true;
			onTimeEnded();
		}
	}

	private void OnDisable()
	{
		EnemyManager em = FindObjectOfType<EnemyManager>();
		if (em != null)
		{
			em.onEnemyDestroyed -= addTime;
		}
	}
}
