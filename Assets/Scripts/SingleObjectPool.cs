﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleObjectPool : Singleton<SingleObjectPool>
{

    private List<GameObject> m_available = new List<GameObject>();
    private List<GameObject> m_inUse = new List<GameObject>();

    private void CreatePool(GameObject i_prefab, int i_initialPoolSize)
    {
        if (i_initialPoolSize <= 0)
            return;

        DestoryPool(); //reset the pool before creating a new one

        bool active = i_prefab.activeSelf;
        i_prefab.SetActive(false);

        while (Instance.m_available.Count < i_initialPoolSize)
        {
            var obj = (GameObject)Instantiate(i_prefab);
            obj.transform.SetParent(transform);
            m_available.Add(obj);
        }

        i_prefab.SetActive(active);

    }

    public static void CreatePoolMain(GameObject i_prefab, int i_initialPoolSize)
    {
        if (Instance != null)
        {
            Instance.CreatePool(i_prefab, i_initialPoolSize);
        }
    }

    public static GameObject Spawn()
    {
        lock (Instance.m_available)
        {
            if (Instance.m_available.Count > 0)
            {
                GameObject obj = Instance.m_available[0];
                Instance.m_inUse.Add(obj);
                Instance.m_available.RemoveAt(0);
                return obj;
            }
            else
            {
                return null;
            }
        }
    }

    public static void Recycle(GameObject i_obj)
    {
        if (i_obj)
        {
            i_obj.gameObject.SetActive(false); //Clean object

            lock (Instance.m_available)
            {
                Instance.m_available.Add(i_obj);
                Instance.m_inUse.Remove(i_obj);
            }
        }
    }

    public static void DestoryPool()
    {
        for (int index = 0; index < Instance.m_available.Count; ++index)
        {
            GameObject.Destroy(Instance.m_available[index]);
        }

        for (int index = 0; index < Instance.m_inUse.Count; ++index)
        {
            GameObject.Destroy(Instance.m_inUse[index]);
        }

        Instance.m_inUse.Clear();
        Instance.m_available.Clear();
    }
}
