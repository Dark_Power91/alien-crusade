﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;

public delegate void OnEnemyEvent();

abstract public class EnemyManager : MonoBehaviour { //Vedere se è necessario un singleton o no
   
    #region Serialized fields
    [SerializeField]
    private Sprite [] m_EnemySprites = null;
    [SerializeField]
    private GameObject m_EnemyPoolPrefab = null;

    [Header("Enemy Spawn")]
    [SerializeField]
    private int m_EnemyAmount = 2;
    [SerializeField]
    private float m_minSpawnOffset = 1f;
    [SerializeField]
    private float m_maxSpawnOffset = 2f;
    [SerializeField]
    private Transform m_ReferencePoint = null;

    [Header("Enemy Movement")]
    [SerializeField]
    private float m_ScrollSpeed = 1f;
    #endregion

    #region Private fields
    private List<Transform> m_EnemyAnchors = new List<Transform>();
    #endregion

    #region Public fields
    public event OnEnemyEvent onEnemyDestroyed = null;
    public event OnEnemyEvent onNextEnemyReady = null;
    #endregion

    void Start()
    { 
        SingleObjectPool.CreatePoolMain(m_EnemyPoolPrefab, m_EnemyAmount + 1);

        for (int index = 0; index < m_EnemyAmount; ++index)
        {
            PoolSpawnNext();
            //SpawnNext();
        }
    }

    abstract protected IDamageable AddDamagable(GameObject i_Enemy);

    private IEnumerator EnemiesScroll()
    {
        // Scroll all enemies to the left. Need to be present at least two enemies
        bool scrollCompleted = false;

        while (!scrollCompleted /*&& m_EnemyAnchors[1]!= null*/)
        {
            float scrollAmount = m_ScrollSpeed * Time.deltaTime;

            if (m_EnemyAnchors.Count > 1)
            {
                Transform enemyTransform = m_EnemyAnchors[1];

                Vector3 currPosition = enemyTransform.position;
                float remainingDistance = (currPosition.x - m_ReferencePoint.position.x);

                Vector3 newPosition = enemyTransform.position;
                newPosition -= new Vector3(scrollAmount, 0f, 0f);

                float newDistance = (newPosition.x - m_ReferencePoint.position.x);

                Vector3 nextPosition;

                if (newDistance < 0f)
                {
                    nextPosition = m_ReferencePoint.position;

                    scrollAmount = remainingDistance;
                    scrollCompleted = true;
                }
                else
                {
                    nextPosition = newPosition;
                }

                enemyTransform.position = newPosition;
            }

            for (int enemyIndex = 2; enemyIndex < m_EnemyAnchors.Count; ++enemyIndex)
            {
                Transform enemyTransform = m_EnemyAnchors[enemyIndex];

                Vector3 newPosition = enemyTransform.position;
                newPosition -= new Vector3(scrollAmount, 0f, 0f);

                enemyTransform.position = newPosition;
            }

            yield return null;
        }
        m_EnemyAnchors.RemoveAt(0);
        PoolSpawnNext();
        // SpawnNext();
        NewEnemyReadyEvent();
    }

    private void EnemyDestroyed(IDamageable i_Enemy)
    {
        i_Enemy.onLifeEnded -= EnemyDestroyed;
        SingleObjectPool.Recycle(i_Enemy.gameObject);
        EnemyDestroyedEvent();
        StartCoroutine("EnemiesScroll");
    }

    private void EnemyDestroyedEvent()
    {
        if (onEnemyDestroyed != null)
        {
            onEnemyDestroyed();
        }

    }

    private void NewEnemyReadyEvent()
    {
        if(onNextEnemyReady != null)
        {
          onNextEnemyReady();
        }
    }

    private void PoolSpawnNext()
    {
        if (m_ReferencePoint == null)
            return;

        // Get Instance from the pool
        GameObject enemyInstance = SingleObjectPool.Spawn();

        //Set Sprite
        SpriteRenderer enemyRenderer = enemyInstance.GetComponent<SpriteRenderer>();
        enemyRenderer.sprite = m_EnemySprites[Random.Range(0, m_EnemySprites.Length)]; ;

        //Set Collider
        BoxCollider2D enemyCollider = enemyInstance.GetComponent<BoxCollider2D>();
        enemyCollider.size = new Vector2(enemyRenderer.sprite.bounds.size.x, enemyRenderer.sprite.bounds.size.y / 2);

        //Set Transform
        enemyInstance.transform.position = NextPosition();
        enemyInstance.transform.rotation = Quaternion.identity;

        //SetActive
        enemyInstance.SetActive(true);
        
        //AddDamageable 
        IDamageable eDamageable = enemyInstance.GetComponent<IDamageable>();
        if(eDamageable == null)
        {
            eDamageable = AddDamagable(enemyInstance);
        }

        //Reset Damageable
        eDamageable.Reset();

        //Subscribe to event
        eDamageable.onLifeEnded += EnemyDestroyed;

        //add to anchors
        m_EnemyAnchors.Add(enemyInstance.transform);
    }

    //private void SpawnNext()
    //{
    //    if (m_EnemyPrefab.Length == 0 || m_ReferencePoint == null)
    //        return;

    //    Vector3 nextPosition;

    //    if (m_EnemyAnchors.Count > 0)
    //    {
    //        Transform lastEnemyTransform = m_EnemyAnchors[m_EnemyAnchors.Count - 1];
    //        nextPosition = lastEnemyTransform.position;

    //        Collider2D lastEnemyCollider = lastEnemyTransform.GetComponent<Collider2D>();
    //        if (lastEnemyCollider != null)
    //        {
    //            nextPosition.x += lastEnemyCollider.bounds.extents.x / 2f;
    //        }

    //        nextPosition.x += Random.Range(m_minSpawnOffset, m_maxSpawnOffset);
    //    }
    //    else
    //    {
    //        nextPosition = m_ReferencePoint.position;
    //    }

    //    GameObject enemyInstance = Instantiate(m_EnemyPrefab[Random.Range(0, m_EnemyPrefab.Length)]);

    //    enemyInstance.name = "Enemy";

    //    enemyInstance.transform.position = nextPosition;
    //    enemyInstance.transform.rotation = Quaternion.identity;

    //    AddDamagable(enemyInstance);

    //    IDamageable eDamageable = enemyInstance.GetComponent<IDamageable>();
    //    eDamageable.onLifeEnded += EnemyDestroyed;

    //    m_EnemyAnchors.Add(enemyInstance.transform);
    //}

    private Vector3 NextPosition()
    {
        Vector3 nextPosition;

        if (m_EnemyAnchors.Count > 0)
        {
            Transform lastEnemyTransform = m_EnemyAnchors[m_EnemyAnchors.Count - 1];
            nextPosition = lastEnemyTransform.position;

            Collider2D lastEnemyCollider = lastEnemyTransform.GetComponent<Collider2D>();

            if (lastEnemyCollider != null)
            {
                nextPosition.x += lastEnemyCollider.bounds.extents.x / 2.0f;
            }
            nextPosition.x += Random.Range(m_minSpawnOffset, m_maxSpawnOffset);
        }
        else
        {
            nextPosition = m_ReferencePoint.position;
        }

        return nextPosition;
    }

    private void OnDisable()
    {
        SingleObjectPool.DestoryPool();
    }
}