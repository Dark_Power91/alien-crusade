﻿using UnityEngine;
public delegate void OnLifeEnded(IDamageable i_Damageable);

public abstract class IDamageable : MonoBehaviour
{
    public event OnLifeEnded onLifeEnded;

    public abstract void TakeDamage(float DamageAmount);

    public abstract void Reset();

    protected void DeathEvent()
    {
        if (onLifeEnded != null)
        {
            onLifeEnded(this);
        }
    }

}
