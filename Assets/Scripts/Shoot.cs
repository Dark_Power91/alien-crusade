﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour 
{
	#region Serialized fields
	[Header("Laser info")]
	[SerializeField]
	private Transform m_startPosition;
	[SerializeField]
	private GameObject m_laser;
	[SerializeField]
	private GameObject m_explosion;
	[SerializeField]
	private float m_laserOnScreenTime;
	[SerializeField]
	private float m_laserDamage = 10.0f;
	#endregion

	#region Private fields
	private float m_lasetTimer;
	private bool m_isLaserCoroutineRunning = false;
    Vector2 m_startScale;
    #endregion

    void Awake()
    {
        m_startScale = m_laser.transform.localScale;
        m_laserDamage = PlayerPrefs.GetFloat("Damage");
    }

    void Update () 
	{
		if (Input.GetButtonDown("Fire1") && Time.timeScale>0) 
		{
			if(!m_isLaserCoroutineRunning) StartCoroutine("laserShoot");
			int layerMsk = 1 << LayerMask.NameToLayer ("Enemy");
			RaycastHit2D hit = Physics2D.Raycast (m_startPosition.position, Vector2.down, 100f, layerMsk);
			if (hit)
			{
                m_laser.transform.position = new Vector2(m_startPosition.position.x, (m_startPosition.position.y));
                m_laser.transform.localScale = new Vector2(1, hit.distance);    
				IDamageable hitted = (hit.collider.gameObject).GetComponent<IDamageable>();
				hitted.TakeDamage(m_laserDamage);
				m_explosion.transform.position = hit.point;
                m_explosion.transform.Rotate(0, 0, Random.Range(0.0f,360.0f));
				m_explosion.SetActive(true);
			}
            else
            {
                m_laser.transform.position = m_startPosition.position;
                m_laser.transform.localScale = m_startScale;
            }

		}
	}

	private IEnumerator laserShoot()
	{
		m_isLaserCoroutineRunning = true;
		m_laser.SetActive(true);
		yield return new WaitForSeconds(m_laserOnScreenTime);
		m_laser.SetActive(false);
		m_explosion.SetActive(false);
		m_isLaserCoroutineRunning = false;
	}

}
