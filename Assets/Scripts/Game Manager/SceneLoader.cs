﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour {

    [SerializeField]
    private int MenuIndex = 0;

    public void OnExitGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(MenuIndex);
    }

    public void OnReloadGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
