﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillCounter : MonoBehaviour {

    #region Serialized Fields
    [SerializeField]
    private Text m_ScoreText;
    [SerializeField]
    private Text m_HighScoreText;
    private int m_KilledEnemies = 0;
    #endregion

    void Awake () {
        FindObjectOfType<EnemyManager>().onEnemyDestroyed += OneKilled;
        FindObjectOfType<TimeBar>().onTimeEnded += OnGameEnded;
	}

    private void OneKilled()
    {
        ++m_KilledEnemies;
    }

    private void OnGameEnded()
    {
        int highScore = PlayerPrefs.GetInt("highScore");
        if(m_KilledEnemies > highScore)
        {
            PlayerPrefs.SetInt("highScore", m_KilledEnemies);
            highScore = m_KilledEnemies;
        }
        m_ScoreText.text = "your crusade is failed\n" + m_KilledEnemies;
        m_HighScoreText.text = "HIGH SCORE\n" + highScore;
    }
}
