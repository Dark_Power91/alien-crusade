﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour {

    #region Serialized Fields
    [SerializeField]
    private GameObject m_GameOverPanel;
    #endregion

    #region Private Fields
    private TimeBar timer;
    #endregion

    void Awake() {
        timer = FindObjectOfType<TimeBar>();
        timer.onTimeEnded += GameOver;
	}
	
	void OnDisable () {
        timer.onTimeEnded -= GameOver;
    }

    void GameOver()
    {
        Time.timeScale = 0;
        m_GameOverPanel.SetActive(true);
    }
}
