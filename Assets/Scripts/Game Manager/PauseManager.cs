﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour {

    [SerializeField]
    private GameObject m_pausePanel;

    public void OnPause()
    {
        Time.timeScale = 0;
        m_pausePanel.SetActive(true);
    }

    public void OnResume()
    {
        Time.timeScale = 1;
        m_pausePanel.SetActive(false);
    }
}
