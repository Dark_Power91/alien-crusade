﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerSelect : MonoBehaviour {

    #region Seriazlied Fileds
    [SerializeField]
    private int m_SkinID;
    [Header("Price to Purchase")]
    [SerializeField]
    private int m_Price;

    [Header ("Player Stats")]
    [SerializeField]
    private float m_Damage;
    [SerializeField]
    private int m_CoinsPerKill;
    [SerializeField]
    private float m_TimePerKill;

    [Header("Player Description")]
    [SerializeField]
    private Button m_PurchaseButton;
    #endregion

    private void OnEnable()
    {
        UpdatePurchaseButton();
    }

    public void OnPLayerSelected()
    {
        if(!GetPurchased())
        {
            int CurrentCoins = PlayerPrefs.GetInt("currencyCoins");
            if (CurrentCoins >= m_Price)
            {
                PlayerPrefs.SetInt("currencyCoins", CurrentCoins - m_Price);
                PlayerPrefs.SetInt("Skin" + m_SkinID, 1);
                UpdatePurchaseButton();
            }
            else
            {
                return;
            }
        }

        PlayerPrefs.SetInt("SkinID", m_SkinID);

        PlayerPrefs.SetFloat("Damage", m_Damage);
        PlayerPrefs.SetInt("Coins", m_CoinsPerKill);
        PlayerPrefs.SetFloat("Time", m_TimePerKill);
    }

    private void UpdatePurchaseButton()
    {
        if(GetPurchased())
        {
            m_PurchaseButton.GetComponentInChildren<Text>().text = "Select";
        }
        else
        {
            m_PurchaseButton.GetComponentInChildren<Text>().text = "Buy\n" + m_Price;
            m_PurchaseButton.interactable = PlayerPrefs.GetInt("currencyCoins") >= m_Price;
        }
    }

    private bool GetPurchased()
    {
        return PlayerPrefs.GetInt("Skin" + m_SkinID) == 1;
    }
}
