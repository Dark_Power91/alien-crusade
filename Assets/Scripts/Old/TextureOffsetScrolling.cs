﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class TextureOffsetScrolling : MonoBehaviour {

	private static float minDistance = 0;
	private static float maxDistance = 100;
	private static float m_scrollDuration = 1;

	[Readonly]
	[SerializeField]
	private float m_scrollSpeed;
	[SerializeField]
	private bool m_contiuosMovement;

	private float m_ActualDuration;
	private Vector2 m_startOffset;
	private float m_lastOffset;
	private float m_scrollOffsetByTime;
	private bool m_moveToggle;

	private Renderer m_renderer;

	void Awake () {
		m_renderer = GetComponent<Renderer>();
		m_startOffset = m_renderer.sharedMaterial.GetTextureOffset("_MainTex");
		m_scrollSpeed = 1 - MathUtils.GetClampedPercentage(transform.position.z, minDistance, maxDistance);
		m_lastOffset = 0.0f;
	}
	
	void Update () {

		//TODO sostituire con un'azione di toggle
		if (Input.GetKey (KeyCode.D) ||Input.GetButtonDown("Fire1")|| Input.GetKey (KeyCode.RightArrow) || Input.touchCount > 0) { 
			m_moveToggle = true;
		}

		if (m_moveToggle|| m_contiuosMovement) 
		{
		
			m_lastOffset = Mathf.Repeat (m_scrollOffsetByTime * m_scrollSpeed, 1);
			Vector2 vOffset = new Vector2 (m_lastOffset, m_startOffset.y);
			m_renderer.sharedMaterial.SetTextureOffset ("_MainTex", vOffset);
			m_ActualDuration += Time.deltaTime;
			m_scrollOffsetByTime += Time.deltaTime;
			if (m_ActualDuration >= m_scrollDuration)
			{
				m_ActualDuration = 0.0f;
				m_moveToggle = false;
			}
		}
		
	}

	void OnDisable()
	{
		m_renderer.sharedMaterial.SetTextureOffset("_MainTex", m_startOffset);
	}
}