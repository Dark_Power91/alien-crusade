﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class PlayerImageSetter : MonoBehaviour {

    [SerializeField]
    private Sprite[] m_Skins;

    private void OnEnable()
    {
        int skinIndex = PlayerPrefs.GetInt("SkinID");
        GetComponent<Image>().sprite = m_Skins[skinIndex];
    }
}
