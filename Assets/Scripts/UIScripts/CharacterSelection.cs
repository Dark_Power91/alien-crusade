﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelection : MonoBehaviour {

    private int PlayerSelected = 0;

    [SerializeField]
    private GameObject[] models;

    private int m_modelsQuantity;

    private void Start()
    {
        m_modelsQuantity = models.Length;
        models[PlayerSelected].SetActive(true);
    }

    public void ArrowRight()
    {
        PlayerSelected++;
        SetPlayer(-1);
    }

    public void ArrowLeft()
    {
        PlayerSelected--;
        SetPlayer(1);

    }

    private void SetPlayer(int prev)
    {
        models[PlayerSelected + prev].SetActive(false);
        PlayerSelected = ComputeModule(PlayerSelected, m_modelsQuantity);
        models[PlayerSelected].SetActive(true);
    }

    private int ComputeModule(int value, int module)
    {
        value += module;
        value %= module;
        return value;
    }
}
