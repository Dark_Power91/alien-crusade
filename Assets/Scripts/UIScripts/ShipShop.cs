﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipShop : MonoBehaviour {

    [SerializeField]
    private GameObject StartMenu;

    [SerializeField]
    private GameObject ShopMenu;
    
    private bool shopMenu = false;

	public void ChangeMenu()
    {
        if(shopMenu == false)
        {
            StartMenu.SetActive(false);
            ShopMenu.SetActive(true);
            shopMenu = true;
        }
        else if(shopMenu == true)
        {
            StartMenu.SetActive(true);
            ShopMenu.SetActive(false);
            shopMenu = false;
        }
    }
}
