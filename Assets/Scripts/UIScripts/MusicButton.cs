﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicButton : MonoBehaviour {

    public Image Icon;

    public Sprite IconOn;
    public Sprite IconOff;

    private bool musicOn = true;

	public void MusicEnabler()
    {
        if(musicOn==true)
        {
            if (Icon != null)
            {
                Icon.sprite = IconOff;
            }

            musicOn = false;
        }
        else if(musicOn==false)
        {
            if (Icon != null)
            {
                Icon.sprite = IconOn;
            }

            musicOn = true;
        }
       
    }
}
