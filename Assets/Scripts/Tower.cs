﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//public delegate void OnLifeEnded(Tower i_Tower);

public class Tower : IDamageable {

    #region Serialized fields
    [SerializeField]
    private float m_totalHealth=200;
    #endregion

    #region Private fields
    private Image m_lifeBar;
    private float m_currentHealth;
    private float m_ratio;
    private float m_startScale;
    #endregion

    private void Awake()
    {
        m_currentHealth = m_totalHealth;
        m_lifeBar = GetComponentInChildren<Image>();
        m_startScale = m_lifeBar.transform.localScale.x;
    }

    public override void TakeDamage(float i_damageAmount)
    {
        m_ratio = m_startScale / (m_totalHealth / i_damageAmount);
        m_lifeBar.rectTransform.localScale = new Vector3((m_lifeBar.transform.localScale.x - m_ratio), m_lifeBar.transform.localScale.y, 1);

        m_currentHealth -= i_damageAmount;
        if(m_currentHealth <= 0.0f)
        {
            DeathEvent();
            //gameObject.SetActive(false); //TEST TODO Attiva skin distrutta
            //Destroy(gameObject);
        }

        Debug.Log("Health:" + m_currentHealth);
    }

    public override void Reset()
    {
        m_currentHealth = m_totalHealth;
        m_lifeBar.rectTransform.localScale = new Vector3(m_startScale, m_lifeBar.transform.localScale.y, 1);
    }

    //public Image Healthbar
    //{
    //    set
    //    {
    //        m_lifeBar = value;
    //        m_startScale = m_lifeBar.transform.localScale.x;
    //    }
    //}

}
